// ACE Advanced Ballistics
force force ace_advanced_ballistics_ammoTemperatureEnabled = true;
force force ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
force force ace_advanced_ballistics_bulletTraceEnabled = true;
force force ace_advanced_ballistics_enabled = true;
force force ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
force force ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Fatigue
force force ace_advanced_fatigue_enabled = true;
force force ace_advanced_fatigue_enableStaminaBar = true;
force force ace_advanced_fatigue_loadFactor = 1;
force force ace_advanced_fatigue_performanceFactor = 3;
force force ace_advanced_fatigue_recoveryFactor = 2;
force force ace_advanced_fatigue_swayFactor = 0.3;
force force ace_advanced_fatigue_terrainGradientFactor = 0.5;

// ACE Advanced Throwing
force force ace_advanced_throwing_enabled = true;
force force ace_advanced_throwing_enablePickUp = true;
force force ace_advanced_throwing_enablePickUpAttached = true;
force force ace_advanced_throwing_showMouseControls = true;
force force ace_advanced_throwing_showThrowArc = true;

// ACE Arsenal
force force ace_arsenal_allowDefaultLoadouts = true;
force force ace_arsenal_allowSharedLoadouts = true;
force force ace_arsenal_camInverted = false;
force force ace_arsenal_enableIdentityTabs = true;
force force ace_arsenal_enableModIcons = true;
force force ace_arsenal_EnableRPTLog = true;
force force ace_arsenal_fontHeight = 4.5;

// ACE Captives
force force ace_captives_allowHandcuffOwnSide = true;
force force ace_captives_allowSurrender = true;
force force ace_captives_requireSurrender = 0;
force force ace_captives_requireSurrenderAi = false;

// ACE Common
force force ace_common_allowFadeMusic = true;
force force ace_common_checkPBOsAction = 2;
force force ace_common_checkPBOsCheckAll = true;
force force ace_common_checkPBOsWhitelist = "";
force force ace_common_displayTextColor = [0,0,0,0.1];
force force ace_common_displayTextFontColor = [1,1,1,1];
force force ace_common_settingFeedbackIcons = 1;
force force ace_common_settingProgressBarLocation = 0;
force force ace_noradio_enabled = true;
force force ace_parachute_hideAltimeter = true;

// ACE Cook off
force force ace_cookoff_ammoCookoffDuration = 0.512477;
force force ace_cookoff_enable = true;
force force ace_cookoff_enableAmmobox = true;
force force ace_cookoff_enableAmmoCookoff = true;
force force ace_cookoff_probabilityCoef = 0.75;

// ACE Explosives
force force ace_explosives_explodeOnDefuse = false;
force force ace_explosives_punishNonSpecialists = true;
force force ace_explosives_requireSpecialist = false;

// ACE Fragmentation Simulation
force force ace_frag_enabled = true;
force force ace_frag_maxTrack = 8;
force force ace_frag_maxTrackPerFrame = 8;
force force ace_frag_reflectionsEnabled = true;
force force ace_frag_spallEnabled = true;

// ACE Goggles
force force ace_goggles_effects = 2;
force force ace_goggles_showInThirdPerson = false;

// ACE Hearing
force force ace_hearing_autoAddEarplugsToUnits = true;
force force ace_hearing_disableEarRinging = false;
force force ace_hearing_earplugsVolume = 0.5;
force force ace_hearing_enableCombatDeafness = true;
force force ace_hearing_enabledForZeusUnits = true;
force force ace_hearing_unconsciousnessVolume = 0.280748;

// ACE Interaction
force force ace_interaction_disableNegativeRating = true;
force force ace_interaction_enableMagazinePassing = true;
force force ace_interaction_enableTeamManagement = true;

// ACE Interaction Menu
force force ace_gestures_showOnInteractionMenu = 2;
force force ace_interact_menu_actionOnKeyRelease = true;
force force ace_interact_menu_addBuildingActions = true;
force force ace_interact_menu_alwaysUseCursorInteraction = true;
force force ace_interact_menu_alwaysUseCursorSelfInteraction = true;
force force ace_interact_menu_colorShadowMax = [0,0,0,1];
force force ace_interact_menu_colorShadowMin = [0,0,0,0.25];
force force ace_interact_menu_colorTextMax = [1,1,1,1];
force force ace_interact_menu_colorTextMin = [1,1,1,0.25];
force force ace_interact_menu_cursorKeepCentered = true;
force force ace_interact_menu_menuAnimationSpeed = 2;
force force ace_interact_menu_menuBackground = 1;
force force ace_interact_menu_selectorColor = [1,0,0];
force force ace_interact_menu_shadowSetting = 1;
force force ace_interact_menu_textSize = 2;
force force ace_interact_menu_useListMenu = true;

// ACE Logistics
force force ace_cargo_enable = true;
force force ace_cargo_loadTimeCoefficient = 5;
force force ace_cargo_paradropTimeCoefficent = 1;
force force ace_rearm_level = 0;
force force ace_rearm_supply = 0;
force force ace_refuel_hoseLength = 30.0059;
force force ace_refuel_rate = 1;
force force ace_repair_addSpareParts = true;
force force ace_repair_autoShutOffEngineWhenStartingRepair = true;
force force ace_repair_consumeItem_toolKit = 0;
force force ace_repair_displayTextOnRepair = true;
force force ace_repair_engineerSetting_fullRepair = 0;
force force ace_repair_engineerSetting_repair = 0;
force force ace_repair_engineerSetting_wheel = 0;
force force ace_repair_fullRepairLocation = 3;
force force ace_repair_repairDamageThreshold = 0.6;
force force ace_repair_repairDamageThreshold_engineer = 0.4;
force force ace_repair_wheelRepairRequiredItems = 0;

// ACE Magazine Repack
force force ace_magazinerepack_timePerAmmo = 1;
force force ace_magazinerepack_timePerBeltLink = 8;
force force ace_magazinerepack_timePerMagazine = 2;

// ACE Map
force force ace_map_BFT_Enabled = false;
force force ace_map_BFT_HideAiGroups = true;
force force ace_map_BFT_Interval = 1;
force force ace_map_BFT_ShowPlayerNames = false;
force force ace_map_DefaultChannel = 0;
force force ace_map_mapGlow = true;
force force ace_map_mapIllumination = true;
force force ace_map_mapLimitZoom = false;
force force ace_map_mapShake = true;
force force ace_map_mapShowCursorCoordinates = false;
force force ace_markers_moveRestriction = 3;

// ACE Map Gestures
force force ace_map_gestures_defaultColor = [1,0.88,0,0.7];
force force ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
force force ace_map_gestures_enabled = true;
force force ace_map_gestures_interval = 0.03;
force force ace_map_gestures_maxRange = 10;
force force ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];

// ACE Map Tools
force force ace_maptools_drawStraightLines = true;
force force ace_maptools_rotateModifierKey = 1;

// ACE Medical
force force ace_medical_ai_enabledFor = 2;
force force ace_medical_AIDamageThreshold = 1;
force force ace_medical_allowLitterCreation = true;
force force ace_medical_allowUnconsciousAnimationOnTreatment = false;
force force ace_medical_amountOfReviveLives = -1;
force force ace_medical_bleedingCoefficient = 0.3;
force force ace_medical_blood_enabledFor = 2;
force force ace_medical_consumeItem_PAK = 1;
force force ace_medical_consumeItem_SurgicalKit = 0;
force force ace_medical_convertItems = 0;
force force ace_medical_delayUnconCaptive = 3;
force force ace_medical_enableAdvancedWounds = true;
force force ace_medical_enableFor = 1;
force force ace_medical_enableOverdosing = true;
force force ace_medical_enableRevive = 2;
force force ace_medical_enableScreams = true;
force force ace_medical_enableUnconsciousnessAI = 2;
force force ace_medical_enableVehicleCrashes = true;
force force ace_medical_healHitPointAfterAdvBandage = true;
force force ace_medical_increaseTrainingInLocations = true;
force force ace_medical_keepLocalSettingsSynced = true;
force force ace_medical_level = 2;
force force ace_medical_litterCleanUpDelay = 180;
force force ace_medical_litterSimulationDetail = 2;
force force ace_medical_maxReviveTime = 600;
force force ace_medical_medicSetting = 2;
force force ace_medical_medicSetting_basicEpi = 0;
force force ace_medical_medicSetting_PAK = 0;
force force ace_medical_medicSetting_SurgicalKit = 0;
force force ace_medical_menu_allow = 0;
force force ace_medical_menu_maxRange = 2;
force force ace_medical_menu_openAfterTreatment = true;
force force ace_medical_menu_useMenu = 0;
force force ace_medical_menuTypeStyle = 0;
force force ace_medical_menuTypeStyleSelf = true;
force force ace_medical_moveUnitsFromGroupOnUnconscious = false;
force force ace_medical_painCoefficient = 0.3;
force force ace_medical_painEffectType = 1;
force force ace_medical_painIsOnlySuppressed = false;
force force ace_medical_playerDamageThreshold = 10;
force force ace_medical_preventInstaDeath = false;
force force ace_medical_remoteControlledAI = true;
force force ace_medical_useCondition_PAK = 0;
force force ace_medical_useCondition_SurgicalKit = 1;
force force ace_medical_useLocation_basicEpi = 0;
force force ace_medical_useLocation_PAK = 3;
force force ace_medical_useLocation_SurgicalKit = 0;

// ACE Mk6 Mortar
force force ace_mk6mortar_airResistanceEnabled = true;
force force ace_mk6mortar_allowCompass = true;
force force ace_mk6mortar_allowComputerRangefinder = false;
force force ace_mk6mortar_useAmmoHandling = true;

// ACE Name Tags
force force ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
force force ace_nametags_nametagColorBlue = [0.67,0.67,1,1];
force force ace_nametags_nametagColorGreen = [0.67,1,0.67,1];
force force ace_nametags_nametagColorMain = [1,1,1,1];
force force ace_nametags_nametagColorRed = [1,0.67,0.67,1];
force force ace_nametags_nametagColorYellow = [1,1,0.67,1];
force force ace_nametags_playerNamesMaxAlpha = 0.75;
force force ace_nametags_playerNamesViewDistance = 5;
force force ace_nametags_showCursorTagForVehicles = true;
force force ace_nametags_showNamesForAI = false;
force force ace_nametags_showPlayerNames = 1;
force force ace_nametags_showPlayerRanks = false;
force force ace_nametags_showSoundWaves = 1;
force force ace_nametags_showVehicleCrewInfo = true;
force force ace_nametags_tagSize = 2;

// ACE Nightvision
force force ace_nightvision_aimDownSightsBlur = 0.3;
force force ace_nightvision_disableNVGsWithSights = false;
force force ace_nightvision_effectScaling = 0.3;
force force ace_nightvision_fogScaling = 0.3;
force force ace_nightvision_noiseScaling = 0.3;
force force ace_nightvision_shutterEffects = true;

// ACE Overheating
force force ace_overheating_displayTextOnJam = true;
force force ace_overheating_enabled = true;
force force ace_overheating_overheatingDispersion = true;
force force ace_overheating_showParticleEffects = true;
force force ace_overheating_showParticleEffectsForEveryone = true;
force force ace_overheating_unJamFailChance = 0.1;
force force ace_overheating_unJamOnreload = false;

// ACE Pointing
force force ace_finger_enabled = true;
force force ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
force force ace_finger_indicatorForSelf = true;
force force ace_finger_maxRange = 10;

// ACE Pylons
force force ace_pylons_enabledForZeus = true;
force force ace_pylons_enabledFromAmmoTrucks = true;
force force ace_pylons_rearmNewPylons = false;
force force ace_pylons_requireEngineer = false;
force force ace_pylons_requireToolkit = true;
force force ace_pylons_searchDistance = 15;
force force ace_pylons_timePerPylon = 5;

// ACE Quick Mount
force force ace_quickmount_distance = 3;
force force ace_quickmount_enabled = true;
force force ace_quickmount_enableMenu = 3;
force force ace_quickmount_priority = 0;
force force ace_quickmount_speed = 18;

// ACE Respawn
force force ace_respawn_removeDeadBodiesDisconnected = false;
force force ace_respawn_savePreDeathGear = false;

// ACE Scopes
force force ace_scopes_correctZeroing = true;
force force ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
force force ace_scopes_defaultZeroRange = 100;
force force ace_scopes_enabled = true;
force force ace_scopes_forceUseOfAdjustmentTurrets = false;
force force ace_scopes_overwriteZeroRange = false;
force force ace_scopes_simplifiedZeroing = false;
force force ace_scopes_useLegacyUI = false;
force force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force force ace_scopes_zeroReferenceHumidity = 0;
force force ace_scopes_zeroReferenceTemperature = 15;

// ACE Spectator
force force ace_spectator_enableAI = false;
force force ace_spectator_restrictModes = 0;
force force ace_spectator_restrictVisions = 0;

// ACE Switch Units
force force ace_switchunits_enableSafeZone = true;
force force ace_switchunits_enableSwitchUnits = false;
force force ace_switchunits_safeZoneRadius = 100;
force force ace_switchunits_switchToCivilian = false;
force force ace_switchunits_switchToEast = false;
force force ace_switchunits_switchToIndependent = false;
force force ace_switchunits_switchToWest = false;

// ACE Tagging
force force ace_tagging_quickTag = 1;

// ACE Uncategorized
force force ace_fastroping_requireRopeItems = false;
force force ace_gforces_enabledFor = 2;
force force ace_hitreactions_minDamageToTrigger = 0.5;
force force ace_inventory_inventoryDisplaySize = 2;
force force ace_laser_dispersionCount = 1;
force force ace_microdagr_mapDataAvailable = 2;
force force ace_microdagr_waypointPrecision = 3;
force force ace_optionsmenu_showNewsOnMainMenu = true;
force force ace_overpressure_distanceCoefficient = 1;

// ACE User Interface
force force ace_ui_allowSelectiveUI = true;
force force ace_ui_ammoCount = false;
force force ace_ui_ammoType = true;
force force ace_ui_commandMenu = true;
force force ace_ui_firingMode = true;
force force ace_ui_groupBar = false;
force force ace_ui_gunnerAmmoCount = true;
force force ace_ui_gunnerAmmoType = true;
force force ace_ui_gunnerFiringMode = true;
force force ace_ui_gunnerLaunchableCount = true;
force force ace_ui_gunnerLaunchableName = true;
force force ace_ui_gunnerMagCount = true;
force force ace_ui_gunnerWeaponLowerInfoBackground = true;
force force ace_ui_gunnerWeaponName = true;
force force ace_ui_gunnerWeaponNameBackground = true;
force force ace_ui_gunnerZeroing = true;
force force ace_ui_magCount = true;
force force ace_ui_soldierVehicleWeaponInfo = true;
force force ace_ui_staminaBar = true;
force force ace_ui_stance = true;
force force ace_ui_throwableCount = true;
force force ace_ui_throwableName = true;
force force ace_ui_vehicleAltitude = true;
force force ace_ui_vehicleCompass = true;
force force ace_ui_vehicleDamage = true;
force force ace_ui_vehicleFuelBar = true;
force force ace_ui_vehicleInfoBackground = true;
force force ace_ui_vehicleName = true;
force force ace_ui_vehicleNameBackground = true;
force force ace_ui_vehicleRadar = true;
force force ace_ui_vehicleSpeed = true;
force force ace_ui_weaponLowerInfoBackground = true;
force force ace_ui_weaponName = true;
force force ace_ui_weaponNameBackground = true;
force force ace_ui_zeroing = true;

// ACE Vehicle Lock
force force ace_vehiclelock_defaultLockpickStrength = 10;
force force ace_vehiclelock_lockVehicleInventory = true;
force force ace_vehiclelock_vehicleStartingLockState = -1;

// ACE View Distance Limiter
force force ace_viewdistance_enabled = true;
force force ace_viewdistance_limitViewDistance = 5000;
force force ace_viewdistance_objectViewDistanceCoeff = 6;
force force ace_viewdistance_viewDistanceAirVehicle = 6;
force force ace_viewdistance_viewDistanceLandVehicle = 0;
force force ace_viewdistance_viewDistanceOnFoot = 0;

// ACE Weapons
force force ace_common_persistentLaserEnabled = true;
force force ace_laserpointer_enabled = true;
force force ace_reload_displayText = true;
force force ace_weaponselect_displayText = true;

// ACE Weather
force force ace_weather_enabled = true;
force force ace_weather_updateInterval = 60;
force force ace_weather_windSimulation = true;

// ACE Wind Deflection
force force ace_winddeflection_enabled = true;
force force ace_winddeflection_simulationInterval = 0.05;
force force ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
force force ace_zeus_autoAddObjects = false;
force force ace_zeus_canCreateZeus = -1;
force force ace_zeus_radioOrdnance = false;
force force ace_zeus_remoteWind = false;
force force ace_zeus_revealMines = 0;
force force ace_zeus_zeusAscension = false;
force force ace_zeus_zeusBird = false;

// ACEX Field Rations
force force acex_field_rations_affectAdvancedFatigue = true;
force force acex_field_rations_enabled = true;
force force acex_field_rations_hudShowLevel = 0;
force force acex_field_rations_hudTransparency = -1;
force force acex_field_rations_hudType = 0;
force force acex_field_rations_hungerSatiated = 1;
force force acex_field_rations_thirstQuenched = 1;
force force acex_field_rations_timeWithoutFood = 4;
force force acex_field_rations_timeWithoutWater = 3;

// ACEX Fortify
force force acex_fortify_settingHint = 2;

// ACEX Headless
force force acex_headless_delay = 15;
force force acex_headless_enabled = true;
force force acex_headless_endMission = 0;
force force acex_headless_log = false;
force force acex_headless_transferLoadout = 1;

// ACEX Sitting
force force acex_sitting_enable = true;

// ACEX View Restriction
force force acex_viewrestriction_mode = 3;
force force acex_viewrestriction_modeSelectiveAir = 1;
force force acex_viewrestriction_modeSelectiveFoot = 1;
force force acex_viewrestriction_modeSelectiveLand = 1;
force force acex_viewrestriction_modeSelectiveSea = 1;
force force acex_viewrestriction_preserveView = false;

// ACEX Volume
force force acex_volume_enabled = false;
force force acex_volume_fadeDelay = 1;
force force acex_volume_lowerInVehicles = false;
force force acex_volume_reduction = 5;
force force acex_volume_remindIfLowered = false;
force force acex_volume_showNotification = true;

// ACRE2
force force acre_sys_core_automaticAntennaDirection = true;
force force acre_sys_core_defaultRadioVolume = 1;
force force acre_sys_core_fullDuplex = true;
force force acre_sys_core_ignoreAntennaDirection = false;
force force acre_sys_core_interference = true;
force force acre_sys_core_postmixGlobalVolume = 2;
force force acre_sys_core_premixGlobalVolume = 2;
force force acre_sys_core_revealToAI = 1;
force force acre_sys_core_spectatorVolume = 1;
force force acre_sys_core_terrainLoss = 0.5;
force force acre_sys_core_ts3ChannelName = "ACRE2";
force force acre_sys_core_ts3ChannelPassword = "fuckyou";
force force acre_sys_core_ts3ChannelSwitch = true;
force force acre_sys_core_unmuteClients = true;
force force acre_sys_signal_signalModel = 2;

// ACRE2 UI
acre_sys_list_CycleRadiosColor = [0.66,0.05,1,1];
acre_sys_list_DefaultPTTColor = [1,0.8,0,1];
acre_sys_list_HintBackgroundColor = [0,0,0,0.8];
acre_sys_list_HintTextFont = "RobotoCondensed";
acre_sys_list_LanguageColor = [1,0.29,0.16,1];
acre_sys_list_PTT1Color = [1,0.8,0,1];
acre_sys_list_PTT2Color = [1,0.8,0,1];
acre_sys_list_PTT3Color = [1,0.8,0,1];
acre_sys_list_SwitchChannelColor = [0.66,0.05,1,1];
acre_sys_list_ToggleHeadsetColor = [0.66,0.05,1,1];

// ACRE2 Zeus
force force acre_sys_zeus_zeusCanSpectate = true;
force force acre_sys_zeus_zeusCommunicateViaCamera = true;
force force acre_sys_zeus_zeusDefaultVoiceSource = false;

// ADV - ACE CPR
force force adv_aceCPR_addTime = 30;
force force adv_aceCPR_AED_stationType = """""Land_Defibrillator_F""""";
force force adv_aceCPR_chance_0 = 25;
force force adv_aceCPR_chance_1 = 25;
force force adv_aceCPR_chance_2 = 25;
force force adv_aceCPR_chance_aed = 90;
force force adv_aceCPR_enable = true;
force force adv_aceCPR_maxTime = 900;
force force adv_aceCPR_useLocation_AED = 0;

// ADV - ACE Splint
force force adv_aceSplint_enable = true;
force force adv_aceSplint_patientCondition = 1;
force force adv_aceSplint_reopenChance_medic = 0;
force force adv_aceSplint_reopenChance_regular = 15;
force force adv_aceSplint_reopenTime = 600;
force force adv_aceSplint_reuseChance = 80;

// AIME Ammo Type Menu
force force UPSL_aime_change_ammo_setting_ammo_class = true;
force force UPSL_aime_change_ammo_setting_vehicle_ammo_class = true;

// AIME General
force force UPSL_aime_setting_hide = true;

// AIME GPS and UAV Terminal
force force UPSL_aime_uav_terminal_setting_gps_action = true;
force force UPSL_aime_uav_terminal_setting_term_action = true;
force force UPSL_aime_uav_terminal_setting_uav_action = true;

// AIME Group Management
force force UPSL_aime_group_setting_drop_leader_action = true;

// AIME Inventory
force force UPSL_aime_inventory_setting_assemble_action = true;
force force UPSL_aime_inventory_setting_backpack_action = true;
force force UPSL_aime_inventory_setting_holder_action = true;
force force UPSL_aime_inventory_setting_open_action = true;

// AIME Vehicle Controls
force force UPSL_aime_vehicle_controls_setting_arty_computer_action = true;
force force UPSL_aime_vehicle_controls_setting_collision_action = true;
force force UPSL_aime_vehicle_controls_setting_engine_action = true;
force force UPSL_aime_vehicle_controls_setting_flaps_action = true;
force force UPSL_aime_vehicle_controls_setting_gear_action = true;
force force UPSL_aime_vehicle_controls_setting_hover_action = true;
force force UPSL_aime_vehicle_controls_setting_lights_action = true;
force force UPSL_aime_vehicle_controls_setting_manual_action = true;
force force UPSL_aime_vehicle_controls_setting_user_actions = true;

// AIME Vehicle Seats
force force UPSL_aime_vehicle_seats_setting_change_action = true;
force force UPSL_aime_vehicle_seats_setting_force_eject = true;
force force UPSL_aime_vehicle_seats_setting_getin_action = true;
force force UPSL_aime_vehicle_seats_setting_getout_action = true;
force force UPSL_aime_vehicle_seats_setting_turnout_action = true;

// Boxloader
force force boxloader_allrepair_height = 5;
force force boxloader_allrepair_load = true;
force force boxloader_allrepair_push = 10000;
force force boxloader_allrepair_weight = 10000;
force force boxloader_allrepair_work = true;
force force boxloader_fort_allow_floating = false;
boxloader_fort_snap_editor = false;
force force boxloader_hidecargo_enabled = true;
force force boxloader_maxload_enabled = false;
force force boxloader_maxload_lift = 50;
force force boxloader_maxload_minpush = 10;
force force boxloader_maxload_overhead = 30;
force force boxloader_maxload_push = 200;
force force boxloader_maxunload_enabled = false;
force force boxloader_preciseunload_enabled = false;
force force boxloader_push_enabled = true;
force force boxloader_retrofit_enabled = true;
force force boxloader_tractor_bulldoze = true;
force force boxloader_tractor_bulldoze_fence = true;
force force boxloader_tractor_bulldoze_hide = true;
force force boxloader_tractor_bulldoze_ruins = true;
force force boxloader_tractor_bulldoze_wall = true;

// Bundeswehr
force force BWA3_Leopard_ClocknumbersDir_Commander = false;
force force BWA3_NaviPad_showMembers = true;
force force BWA3_Puma_ClocknumbersDir_Commander = false;
force force BWA3_Puma_ClocknumbersDir_Gunner = false;

// CBA UI
cba_ui_notifyLifetime = 4;
cba_ui_StorePasswords = 1;

// CBA Weapons
force force cba_disposable_dropUsedLauncher = 2;
force force cba_disposable_replaceDisposableLauncher = true;
cba_events_repetitionMode = 1;
cba_optics_usePipOptics = true;

// Diwako Stalker Like Anomalies
force force ANOMALY_DEBUG = false;
force force ANOMALY_DETECTION_RANGE = 20;
force force ANOMALY_DETECTOR_ITEM = "AnomalyDetector";
force force ANOMALY_GAS_MASKS = "GP5_RaspiratorPS,GP5Filter_RaspiratorPS,GP7_RaspiratorPS,GP21_GasmaskPS,SE_S10,G_Respirator_white_F,MASK_M40_OD,MASK_M40,MASK_M50";
force force ANOMALY_IDLE_DISTANCE = 350;
force force ANOMALY_TRIGGER_DISTANCE = 300;
force force diwako_anomalies_enable = true;

// Diwako's ACE Ragdolling
force force diwako_ragdoll_ai = true;
force force diwako_ragdoll_ragdolling = true;
force force diwako_ragdoll_server_only = false;

// DUI - Squad Radar - Indicators
force force diwako_dui_indicators_fov_scale = true;
force force diwako_dui_indicators_icon_buddy = true;
force force diwako_dui_indicators_icon_leader = true;
force force diwako_dui_indicators_icon_medic = true;
force force diwako_dui_indicators_range = 30;
force force diwako_dui_indicators_range_scale = true;
force force diwako_dui_indicators_show = true;
force force diwako_dui_indicators_size = 1;
force force diwako_dui_indicators_style = "standard";
force force diwako_dui_indicators_useACENametagsRange = true;

// DUI - Squad Radar - Main
force force diwako_dui_ace_hide_interaction = true;
force force diwako_dui_colors = "ace";
force force diwako_dui_font = "RobotoCondensed";
force force diwako_dui_icon_style = "nato";
force force diwako_dui_main_hide_ui_by_default = false;
force force diwako_dui_main_squadBlue = [0,0,1,1];
force force diwako_dui_main_squadGreen = [0,1,0,1];
force force diwako_dui_main_squadMain = [1,1,1,1];
force force diwako_dui_main_squadRed = [1,0,0,1];
force force diwako_dui_main_squadYellow = [1,1,0,1];

// DUI - Squad Radar - Radar
force force diwako_dui_compass_hide_alone_group = false;
force force diwako_dui_compass_hide_blip_alone_group = false;
force force diwako_dui_compass_icon_scale = 1;
force force diwako_dui_compass_opacity = 1;
force force diwako_dui_compass_style = ["\z\diwako_dui\addons\radar\UI\compass_styles\classic\limited.paa","\z\diwako_dui\addons\radar\UI\compass_styles\classic\full.paa"];
force force diwako_dui_compassRange = 30;
force force diwako_dui_compassRefreshrate = 0;
force force diwako_dui_dir_showMildot = true;
force force diwako_dui_dir_size = 1;
force force diwako_dui_distanceWarning = 3;
force force diwako_dui_enable_compass = true;
force force diwako_dui_enable_compass_dir = 2;
force force diwako_dui_enable_occlusion = true;
force force diwako_dui_enable_occlusion_cone = 360;
force force diwako_dui_hudScaling = 1;
force force diwako_dui_namelist = true;
force force diwako_dui_namelist_bg = 0;
force force diwako_dui_namelist_only_buddy_icon = false;
force force diwako_dui_namelist_size = 0.75;
force force diwako_dui_namelist_text_shadow = 0;
force force diwako_dui_namelist_width = 100;
force force diwako_dui_radar_ace_finger = true;
force force diwako_dui_radar_group_by_vehicle = false;
force force diwako_dui_radar_leadingZeroes = true;
force force diwako_dui_radar_namelist_hideWhenLeader = false;
force force diwako_dui_radar_namelist_vertical_spacing = 1;
force force diwako_dui_radar_occlusion_fade_time = 10;
force force diwako_dui_radar_pointer_color = [1,0.5,0,1];
force force diwako_dui_radar_pointer_style = "standard";
force force diwako_dui_radar_sortType = "name";
force force diwako_dui_radar_sqlFirst = true;
force force diwako_dui_radar_trackingColor = [0.93,0.26,0.93,1];
force force diwako_dui_reset_ui_pos = false;
force force diwako_dui_use_layout_editor = true;

// dzn Extended Jamming
force force dzn_EJAM_dud_ChanceSettings = 30;
force force dzn_EJAM_fail_to_eject_ChanceSettings = 20;
force force dzn_EJAM_fail_to_extract_ChanceSettings = 20;
force force dzn_EJAM_feed_failure_2_ChanceSettings = 20;
force force dzn_EJAM_feed_failure_ChanceSettings = 30;
force force dzn_EJAM_Force = true;
force force dzn_EJAM_ForceOverallChance = true;
force force dzn_EJAM_MappingSettings = "[""""arifle_MX_F"""",0.05,45,10,45,0,0]";
force force dzn_EJAM_OverallChanceSetting = 0.01;
force force dzn_EJAM_SubsonicJamEffectSetting = "20";

// GRAD Trenches
force force grad_trenches_functions_allowBigEnvelope = true;
force force grad_trenches_functions_allowCamouflage = true;
force force grad_trenches_functions_allowDigging = true;
force force grad_trenches_functions_allowGiantEnvelope = true;
force force grad_trenches_functions_allowShortEnvelope = true;
force force grad_trenches_functions_allowSmallEnvelope = true;
force force grad_trenches_functions_allowVehicleEnvelope = true;
force force grad_trenches_functions_bigEnvelopeDigTime = 20;
force force grad_trenches_functions_buildFatigueFactor = 1;
force force grad_trenches_functions_camouflageRequireEntrenchmentTool = false;
force force grad_trenches_functions_enableAutomaticFilePath = true;
force force grad_trenches_functions_giantEnvelopeDigTime = 45;
force force grad_trenches_functions_shortEnvelopeDigTime = 10;
force force grad_trenches_functions_smallEnvelopeDigTime = 15;
force force grad_trenches_functions_stopBuildingAtFatigueMax = true;
force force grad_trenches_functions_vehicleEnvelopeDigTime = 60;

// LAxemann's Suppress
force force L_Suppress_buildup = 1;
force force L_Suppress_enabled = true;
force force L_Suppress_flyByEffects = true;
force force L_Suppress_flyByIntensity = 1;
force force L_Suppress_halting = true;
force force L_Suppress_intensity = 1;
force force L_Suppress_playerSwabEnabled = true;
force force L_Suppress_recovery = 3;

// Metis Marker
mts_markers_saveLastSelection = true;
mts_markers_showPresetsUI = true;

// SHOL's KnockingVehicle
force force shol_KnockingVehicle_Debug = false;
force force shol_KnockingVehicle_NoticeCrew = true;
force force shol_KnockingVehicle_Range = 4;
force force shol_KnockingVehicle_Volume = 3.5;

// TacSalmon Buttstroke
force force Salmon_bs_ff = true;
force force Salmon_bs_rd = true;

// Turret Enhanced
force force Fat_Lurch_Grid = true;
force force Fat_Lurch_GridNum = 10;
force force Fat_Lurch_MapSlew = true;
force force Fat_Lurch_Markers = true;
force force Fat_Lurch_Measure = true;
force force Fat_Lurch_ShowNorth = true;
force force Fat_Lurch_ShowTarget = true;

// VET_Unflipping
force force vet_unflipping_require_serviceVehicle = false;
force force vet_unflipping_require_toolkit = false;
force force vet_unflipping_time = 5;
force force vet_unflipping_unit_man_limit = 10;
force force vet_unflipping_unit_mass_limit = 5000;
force force vet_unflipping_vehicle_mass_limit = 100000;

// Vurtual's Vehicles
force force vurtual_base_fording_damage = false;
force force vurtual_m998_enableTow = true;
force force vurtual_m998_trailer_snap = 20;

// Zeus Enhanced
force force zen_camera_adaptiveSpeed = true;
force force zen_camera_defaultSpeedCoef = 1;
force force zen_camera_fastSpeedCoef = 1;
force force zen_camera_followTerrain = true;
force force zen_common_autoAddObjects = true;
force force zen_common_darkMode = true;
force force zen_common_disableGearAnim = true;
force force zen_common_preferredArsenal = 1;
force force zen_context_menu_enabled = 2;
force force zen_editor_declutterEmptyTree = true;
force force zen_editor_disableLiveSearch = true;
force force zen_editor_moveDisplayToEdge = true;
force force zen_editor_removeWatermark = true;
force force zen_editor_unitRadioMessages = 0;
force force zen_visibility_enabled = true;
force force zen_vision_enableBlackHot = false;
force force zen_vision_enableBlackHotGreenCold = false;
force force zen_vision_enableBlackHotRedCold = false;
force force zen_vision_enableGreenHotCold = false;
force force zen_vision_enableNVG = true;
force force zen_vision_enableRedGreenThermal = false;
force force zen_vision_enableRedHotCold = false;
force force zen_vision_enableWhiteHot = true;
force force zen_vision_enableWhiteHotRedCold = false;
