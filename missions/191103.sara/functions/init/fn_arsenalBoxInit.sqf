private ["_box"];
_box = _this select 0;

_box allowDamage false;
clearWeaponCargoGlobal _box;
clearMagazineCargoGlobal _box;
clearBackpackCargoGlobal _box;
clearItemCargoGlobal _box;

vanillaArsenalBoxAction = _box addaction ["<t color='#FFD700'>Vanilla Arsenal</t>",{[player,true] call BIS_fnc_arsenal;}];

aceArsenalBoxAction = _box addaction ["<t color='#FFD700'>ACE3 Arsenal</t>",{[player, player, true] call ace_arsenal_fnc_openBox;}];

openUagArsenalAction = _box addaction ["<t color='#FFD700'>UAG Loadouts</t>",{createDialog "ars_Dialog";}];

[_box, true] call ace_arsenal_fnc_initBox;

callForReinsertAction = _box addaction ["<t color='#89CFF0'>Request Reinsert</t>",{[] call uag_fnc_playerRequestPickup;}];
