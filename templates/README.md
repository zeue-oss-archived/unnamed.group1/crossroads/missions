# Mission Naming Legend

## Types
- uag_coXX_NAME_SIDE_FACTION_ATTR.MAP
- uag_pvpXX-YY-ZZ_NAME.MAP
## Attributes
- e=empty_mission
- f=with_prebuilt_fob
- C=with_carrier
- L=with_premade_loadouts
- c=with_civilians
## Factions
- virtual=no faction, blue boys!
- swe80=swedish armed forces (1980-1990)
- chad=chadian armed forces
- usaf=united states armed forces