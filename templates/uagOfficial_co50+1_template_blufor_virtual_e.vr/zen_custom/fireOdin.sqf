["Fire Support", "ODIN-2 Satellite",
	{
		[_this select 0] spawn {
			_deployAlt = _this select 0; 
			_deployAlt set [2, (_deployAlt select 2) + 1250];
			_payload = "Land_BakedBeans_F" createVehicle _deployAlt;
			_payload setPosASL _deployAlt;
			_payload setVelocity [0,0,-250];
			
			_eff1 = "#lightpoint" createVehicle getpos _payload;
			_eff1 setLightBrightness 40;
			_eff1 setLightDayLight true;
			_eff1 setLightAmbient[1,0,0];
			_eff1 setLightColor[1, 0, 0];
			_eff1 lightAttachObject [_payload, [0,0,0.1]];
			
			_eff2  = "#particlesource" createVehicleLocal getPosASL _payload;
			_eff2  setParticleCircle [0,[0,0,0]];
			_eff2  setParticleRandom [0,[0,0,0],[0,0,0],0,0,[0,0,0,0],0,0];
			_eff2  setParticleParams [["\A3\data_f\ParticleEffects\Universal\Universal",16,3,17,1], "", "Billboard", 1,0.5,[0, 0, 0], [0, 0, 0.5],5, 10.1, 7.9, 0.0001, [7,3,1], [[0.1,0.1,0.1,0.9], [0.6,0.6,0.6,0.6], [0.8,0.8,0.8,0.4],[0.9,0.9,0.9,0.3],[1,1,1,0.1]],[500], 1, 0, "", "", _payload];
			_eff2  setDropInterval 0.002;
			
			while {(getPosATL _payload) select 2 > 100} do {
				sleep 0.35;
				_oldZV = (velocity _payload) select 2;
				_newZV = _oldZV - 50;
				_payload setVelocity [0,0,_newZV];
			};
			
			waitUntil {(getPosATL _payload) select 2 < 100 && (velocity _payload) select 2 > -1 };
			
			"Bomb_04_F" createVehicle getPosATL _payload;
			
			deleteVehicle _eff1;
			deleteVehicle _eff2;
			deleteVehicle _payload;
		}
	}
] call zen_custom_modules_fnc_register;