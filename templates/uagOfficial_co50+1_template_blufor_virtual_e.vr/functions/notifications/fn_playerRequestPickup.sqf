private ["_playerName", "_playerLocation", "_alertText"];
_playerName = name player;
_playerLocation = mapGridPosition getPos player;

_alertText = _playerName + " has requested reinsert via the arsenal box at " + _playerLocation;

["Alert", [_alertText]] remoteExec ["BIS_fnc_showNotification"];